module.exports = (sequelize, Sequelize) => {
    const Article = sequelize.define("article", {
      title: {
        type: Sequelize.STRING
      },
      body: {
        type: Sequelize.STRING
      }
    });
    return Article;
  };
  