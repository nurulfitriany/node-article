module.exports = {
    HOST: "localhost",
    USER: "gorm",
    PASSWORD: "gorm",
    DB: "gorm",
    dialect: "postgres",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
};
