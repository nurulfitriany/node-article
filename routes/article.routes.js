const { authJwt } = require("../middleware");
const controller = require("../controllers/article.controller");
module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.get(  
    "/api/articles", 
    controller.listArticles
  );
  app.get(
    "/api/article/:id",
    [authJwt.verifyToken],
    controller.detailArticle
  );
  app.post(
    "/api/article",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.CreateArticles
  );
  app.put(
    "/api/article/:id",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.updateArticle
  );
  app.delete(
    "/api/article/:id",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.deleteArticle
  )  
}