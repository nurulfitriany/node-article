const { article } = require("../models");
const db = require("../models");
const Article = db.article;

exports.CreateArticles = (req, res) => {
    Article
    .create({
        title: req.body.title,
        body: req.body.body,
        userId: req.userId
    })
    .then(article => {
        res
          .status(200)
          .send({ 
              data: article,
              message: "Create Article Successfully Created!" 
          });
    })
    .catch(err => {
      res.status(500).send({ message: err.message })
    })
}
exports.listArticles = (req, res) => {
    Article
    .findAll()  
    .then(article => {
      if (!article) {
        return res.status(404).send({ message: "Articles Not Found." });
      }
  
      res.status(200).send(article);
    })
    .catch(err => {
      res.status(500).send({ message: err.message })
    })
}

exports.detailArticle = (req, res) => {
    Article
    .findOne({
        where: {
          id: req.params.id
        }
    })
    .then(article => {
        if (!article) {
          return res.status(404).send({ message: "Detail Article Not Found." });
        }

        res.status(200).send(article)
    })
    .catch(err => {
        res.status(500).send({ message: err.message })
      })
}

exports.updateArticle = (req, res) => {
    Article
    .findByPk(req.params.id)
    .then(article => {
        if (!article) {
           return res.status(404).send({ message: "Article Not Found." });
        }

        article.update({
            title: req.body.title || article.title,
            body: req.body.body || article.body,
        })
        .then(() => res.status(200).send(article))
        .catch((error) => res.status(400).send(error))
    })
    .catch(err => {
        res.status(500).send({ message: err.message })
      })
}

exports.deleteArticle = (req, res) => {
    Article
    .findByPk(req.params.id)
    .then(article => {
        if (!article) {
           return res.status(404).send({ message: "Article Not Found." });
        }

        article.destroy()
        .then(() => res.status(204).send({ message: "Successfully Deleted" }))
        .catch((error) => res.status(400).send(error));
    })
    .catch(err => {
        res.status(500).send({ message: err.message })
      })
}